package pl.awalas.json.chees.model;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.LinkedList;

import pl.awalas.json.chees.R;

/**
 * Created by JSON on 31.05.15.
 */
public class Piece implements View.OnClickListener {

	public static final int PAWN = 0;
	public static final int BISHOP = 1;
	public static final int KNIGHT = 2;
	public static final int ROOK = 3;
	public static final int QUEEN = 4;
	public static final int KING = 5;

	private AlertDialog dialog;
	private int figureType;
	private int grade;
	private boolean white;
	private boolean firstMove;
	private boolean promoted;
	private Pair<Integer, Integer> currentPosition;
	private LinkedList<Pair<Integer, Integer>> availableMoves;

	public Piece(int figureType, boolean white, Pair<Integer, Integer> currentPosition) {
		this.figureType = figureType;
		this.white = white;
		this.currentPosition = currentPosition;
	}

	public int getFigureType() {
		return figureType;
	}

	public void setFigureType(int figureType) {
		this.figureType = figureType;
	}

	public boolean isWhite() {
		return white;
	}

	public void setWhite(boolean white) {
		this.white = white;
	}

	public boolean isFirstMove() {
		return firstMove;
	}

	public void setFirstMove(boolean firstMove) {
		this.firstMove = firstMove;
	}

	public boolean isPromoted() {
		return promoted;
	}

	public void setPromoted(boolean promoted) {
		this.promoted = promoted;
	}

	public Pair<Integer, Integer> getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(Pair<Integer, Integer> currentPosition) {
		this.currentPosition = currentPosition;
	}

	public LinkedList<Pair<Integer, Integer>> getAvailableMoves(Piece[][] board) {
		return getAvailableMoves(board, false);
	}

	public LinkedList<Pair<Integer, Integer>> getAvailableMoves(Piece[][] board, boolean
			generate) {
		if (generate) {
			switch (figureType) {
				case PAWN:
					availableMoves = getPawnMoves(board);
					break;
				case BISHOP:
					availableMoves = getBishopMoves(board);
					break;
				case KNIGHT:
					availableMoves = getKnightMoves(board);
					break;
				case ROOK:
					availableMoves = getRookMoves(board);
					break;
				case QUEEN:
					availableMoves = getQueenMoves(board);
					break;
				case KING:
					availableMoves = getKingMoves(board);
					break;
			}
		}
		return availableMoves;
	}

	public void setAvailableMoves(LinkedList<Pair<Integer, Integer>> availableMoves) {
		this.availableMoves = availableMoves;
	}

	public boolean move(Context context, Pair<Integer, Integer> position) {
		return move(context, position, false);
	}

	public boolean move(Context context, Pair<Integer, Integer> position, boolean computerMove) {
		if (availableMoves.contains(position)) {
			if (figureType == PAWN &&
					((position.first == 7 && white) || (position.first == 0 && !white))) {
				promotion(context, computerMove);
			}
			firstMove = false;
			currentPosition = position;
			return true;
		}
		return false;
	}

	@Override public String toString() {
		switch (figureType) {
			case BISHOP:
				return !white ? "♝" : "♗";
			case KING:
				return !white ? "♚" : "♔";
			case KNIGHT:
				return !white ? "♞" : "♘";
			case PAWN:
				return !white ? "♟" : "♙";
			case QUEEN:
				return !white ? "♛" : "♕";
			case ROOK:
				return !white ? "♜" : "♖";
			default:
				return " ";
		}
	}

	private void promotion(Context context, boolean auto) {
		if (auto) {
			figureType = QUEEN;
		} else {
			createPromotionAlertDialog(context);
		}
	}

	private void createPromotionAlertDialog(Context context) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("Wybierz nowy typ figury");
		LayoutInflater inflater = LayoutInflater.from(context);
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.alert_promotion, null);
		Button queen = (Button) layout.findViewById(R.id.queen);
		Button bishop = (Button) layout.findViewById(R.id.bishop);
		Button knight = (Button) layout.findViewById(R.id.knight);
		Button rook = (Button) layout.findViewById(R.id.rook);
		if (!white) {
			queen.setText("♛");
			bishop.setText("♝");
			knight.setText("♞");
			rook.setText("♜");
		} else {
			queen.setText("♕");
			bishop.setText("♗");
			knight.setText("♘");
			rook.setText("♖");
		}
		queen.setOnClickListener(this);
		bishop.setOnClickListener(this);
		knight.setOnClickListener(this);
		rook.setOnClickListener(this);
		alertDialog.setView(layout);
		dialog = alertDialog.create();
		dialog.show();
	}

	@Override public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bishop:
				figureType = BISHOP;
				break;
			case R.id.knight:
				figureType = KNIGHT;
				break;
			case R.id.rook:
				figureType = ROOK;
				break;
			case R.id.queen:
				figureType = QUEEN;
				break;
		}
		dialog.dismiss();
	}

	private LinkedList<Pair<Integer, Integer>> getRookMoves(Piece[][] board) {
		LinkedList<Pair<Integer, Integer>> result = new LinkedList<>();
		boolean end = false;
		/* w dół */
		for (int i = currentPosition.first + 1; i < 8 && !end; i++) {
			if (board[i][currentPosition.second] == null) {
				result.add(new Pair<>(i, currentPosition.second));
			} else {
				if (board[i][currentPosition.second].isWhite() != white) {
					result.add(new Pair<>(i, currentPosition.second));
				}
				end = true;
			}
		}
		end = false;
		/* w górę */
		for (int i = currentPosition.first - 1; i > -1 && !end; i--) {
			if (board[i][currentPosition.second] == null) {
				result.add(new Pair<>(i, currentPosition.second));
			} else {
				if (board[i][currentPosition.second].isWhite() != white) {
					result.add(new Pair<>(i, currentPosition.second));
				}
				end = true;
			}
		}
		end = false;
		/* w prawo */
		for (int i = currentPosition.second + 1; i < 8 && !end; i++) {
			if (board[currentPosition.first][i] == null) {
				result.add(new Pair<>(currentPosition.first, i));
			} else {
				if (board[currentPosition.first][i].isWhite() != white) {
					result.add(new Pair<>(currentPosition.first, i));
				}
				end = true;
			}
		}
		end = false;
		/* w lewo */
		for (int i = currentPosition.second - 1; i > -1 && !end; i--) {
			if (board[currentPosition.first][i] == null) {
				result.add(new Pair<>(currentPosition.first, i));
			} else {
				if (board[currentPosition.first][i].isWhite() != white) {
					result.add(new Pair<>(currentPosition.first, i));
				}
				end = true;
			}
		}
		return result;
	}

	private LinkedList<Pair<Integer, Integer>> getKingMoves(Piece[][] board) {
		LinkedList<Pair<Integer, Integer>> result = new LinkedList<>();
		if (firstMove) {
			/* rozszady */
			/*if (board[currentPosition.first][0] != null &&
					board[currentPosition.first][0].getFigureType() == ROOK &&
					board[currentPosition.first][0].isWhite() == white &&
					board[currentPosition.first][0].isFirstMove() &&
					board[currentPosition.first][1] == null &&
					board[currentPosition.first][2] == null &&
					board[currentPosition.first][3] == null) {
				result.add(new Pair<>(currentPosition.first, 2));
			}
			if (board[currentPosition.first][7] != null &&
					board[currentPosition.first][7].getFigureType() == ROOK &&
					board[currentPosition.first][7].isWhite() == white &&
					board[currentPosition.first][7].isFirstMove() &&
					board[currentPosition.first][6] == null &&
					board[currentPosition.first][5] == null) {
				result.add(new Pair<>(currentPosition.first, 6));
			}*/
		}
		/* przejście do rzedu powyżej */
		if ((currentPosition.first - 1) > -1) {
			if ((currentPosition.second - 1) > -1 &&
					(board[currentPosition.first - 1][currentPosition.second - 1] == null ||
							board[currentPosition.first - 1][currentPosition.second - 1]
									.isWhite() != white)) {
				result.add(new Pair<>(currentPosition.first - 1, currentPosition.second - 1));
			}
			if ((currentPosition.second + 1) < 8 &&
					(board[currentPosition.first - 1][currentPosition.second + 1] == null ||
							board[currentPosition.first - 1][currentPosition.second + 1]
									.isWhite() != white)) {
				result.add(new Pair<>(currentPosition.first - 1, currentPosition.second + 1));
			}
			if (board[currentPosition.first - 1][currentPosition.second] == null ||
					board[currentPosition.first - 1][currentPosition.second].isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - 1, currentPosition.second));
			}
		}
		/* przejście do rzedu poniżej */
		if ((currentPosition.first + 1) < 8) {
			if ((currentPosition.second - 1) > -1 &&
					(board[currentPosition.first + 1][currentPosition.second - 1] == null ||
							board[currentPosition.first + 1][currentPosition.second - 1].
									isWhite() != isWhite())) {
				result.add(new Pair<>(currentPosition.first + 1, currentPosition.second - 1));
			}
			if ((currentPosition.second + 1) < 8 &&
					(board[currentPosition.first + 1][currentPosition.second + 1] == null ||
							board[currentPosition.first + 1][currentPosition.second + 1].
									isWhite() != isWhite())) {
				result.add(new Pair<>(currentPosition.first + 1, currentPosition.second + 1));
			}
			if (board[currentPosition.first + 1][currentPosition.second] == null ||
					board[currentPosition.first + 1][currentPosition.second].
							isWhite() != isWhite()) {
				result.add(new Pair<>(currentPosition.first + 1, currentPosition.second));
			}
		}
		/* przejście w rzędzie w prawo */
		if ((currentPosition.second + 1) < 8) {
			if (board[currentPosition.first][currentPosition.second + 1] == null ||
					board[currentPosition.first][currentPosition.second + 1].
							isWhite() != isWhite()) {
				result.add(new Pair<>(currentPosition.first, currentPosition.second + 1));
			}
		}
		/* przejście w rzędzie w lewo */
		if ((currentPosition.second - 1) > -1) {
			if (board[currentPosition.first][currentPosition.second - 1] == null ||
					board[currentPosition.first][currentPosition.second - 1].
							isWhite() != isWhite()) {
				result.add(new Pair<>(currentPosition.first, currentPosition.second - 1));
			}
		}
		return result;
	}

	private LinkedList<Pair<Integer, Integer>> getPawnMoves(Piece[][] board) {
		LinkedList<Pair<Integer, Integer>> result = new LinkedList<>();
		int direction = white ? 1 : -1;
		/* ruch o dwa pola */
		if (!white) {
			if (currentPosition.first == 6 && board[5][currentPosition.second] == null &&
					board[4][currentPosition.second] == null) {
				result.add(new Pair<>(4, currentPosition.second));
			}
		} else {
			if (currentPosition.first == 1 && board[2][currentPosition.second] == null &&
					board[3][currentPosition.second] == null) {
				result.add(new Pair<>(3, currentPosition.second));
			}
		}
		/* bicie w lewo */
		if ((currentPosition.second - 1 > -1) &&
				(currentPosition.first + direction) > -1 &&
				(currentPosition.first + direction) < 8 &&
				board[currentPosition.first + direction][currentPosition.second - 1] != null &&
				white != board[currentPosition.first + direction][currentPosition.second - 1]
						.isWhite()) {
			result.add(
					new Pair<>((currentPosition.first + direction), (currentPosition.second - 1)));
		}
		/* bicie w prawo */
		if ((currentPosition.second + 1 < 8) && (currentPosition.first + direction) > -1 &&
				(currentPosition.first + direction) < 8 &&
				board[currentPosition.first + direction][currentPosition.second + 1] != null &&
				white != board[currentPosition.first + direction][currentPosition.second + 1]
						.isWhite()) {
			result.add(
					new Pair<>((currentPosition.first + direction), (currentPosition.second + 1)));
		}
		/* poruszanie się */
		if (currentPosition.first + direction > -1 &&
				currentPosition.first + direction < 8 &&
				board[currentPosition.first + direction][currentPosition.second] == null) {
			result.add(new Pair<>((currentPosition.first + direction), currentPosition.second));
		}
		return result;
	}

	private LinkedList<Pair<Integer, Integer>> getKnightMoves(Piece[][] board) {
		LinkedList<Pair<Integer, Integer>> result = new LinkedList<>();
		if (currentPosition.first + 1 < 8 && currentPosition.second + 2 < 8) {
			if (board[currentPosition.first + 1][currentPosition.second + 2] == null ||
					board[currentPosition.first + 1][currentPosition.second + 2].isWhite() !=
							white) {
				result.add(new Pair<>(currentPosition.first + 1, currentPosition.second + 2));
			}
		}
		if ((currentPosition.first + 2) < 8 && (currentPosition.second + 1) < 8) {
			if (board[currentPosition.first + 2][currentPosition.second + 1] == null ||
					board[currentPosition.first + 2][currentPosition.second + 1].isWhite() !=
							white) {
				result.add(new Pair<>(currentPosition.first + 2, currentPosition.second + 1));
			}
		}
		if (currentPosition.first - 1 > -1 && currentPosition.second + 2 < 8) {
			if (board[currentPosition.first - 1][currentPosition.second + 2] == null ||
					board[currentPosition.first - 1][currentPosition.second + 2].
							isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - 1, currentPosition.second + 2));
			}
		}
		if (currentPosition.first - 2 > -1 && currentPosition.second + 1 < 8) {
			if (board[currentPosition.first - 2][currentPosition.second + 1] == null ||
					board[currentPosition.first - 2][currentPosition.second + 1].
							isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - 2, currentPosition.second + 1));
			}
		}
		if (currentPosition.first + 1 < 8 && currentPosition.second - 2 > -1) {
			if (board[currentPosition.first + 1][currentPosition.second - 2] == null ||
					board[currentPosition.first + 1][currentPosition.second - 2].
							isWhite() != white) {
				result.add(new Pair<>(currentPosition.first + 1, currentPosition.second - 2));
			}
		}
		if (currentPosition.first + 2 < 8 && currentPosition.second - 1 > -1) {
			if (board[currentPosition.first + 2][currentPosition.second - 1] == null ||
					board[currentPosition.first + 2][currentPosition.second - 1].
							isWhite() != white) {
				result.add(new Pair<>(currentPosition.first + 2, currentPosition.second - 1));
			}
		}
		if (currentPosition.first - 1 > -1 && currentPosition.second - 2 > -1) {
			if (board[currentPosition.first - 1][currentPosition.second - 2] == null ||
					board[currentPosition.first - 1][currentPosition.second - 2].
							isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - 1, currentPosition.second - 2));
			}
		}
		if ((currentPosition.first - 2) >= 0 && (currentPosition.second - 1) >= 0) {
			if (board[currentPosition.first - 2][currentPosition.second - 1] == null ||
					board[currentPosition.first - 2][currentPosition.second - 1].
							isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - 2, currentPosition.second - 1));
			}
		}
		return result;
	}

	private LinkedList<Pair<Integer, Integer>> getQueenMoves(Piece[][] board) {
		LinkedList<Pair<Integer, Integer>> result = new LinkedList<>();
		boolean end = false;
		/* w dół */
		for (int i = currentPosition.first + 1; i < 8 && !end; i++) {
			if (board[i][currentPosition.second] == null) {
				result.add(new Pair<>(i, currentPosition.second));
			} else {
				if (board[i][currentPosition.second].isWhite() != white) {
					result.add(new Pair<>(i, currentPosition.second));
				}
				end = true;
			}
		}
		end = false;
		/* w górę */
		for (int i = currentPosition.first - 1; i > -1 && !end; i--) {
			if (board[i][currentPosition.second] == null) {
				result.add(new Pair<>(i, currentPosition.second));
			} else {
				if (board[i][currentPosition.second].isWhite() != white) {
					result.add(new Pair<>(i, currentPosition.second));
				}
				end = true;
			}
		}
		end = false;
		/* w prawo */
		for (int i = currentPosition.second + 1; i < 8 && !end; i++) {
			if (board[currentPosition.first][i] == null) {
				result.add(new Pair<>(currentPosition.first, i));
			} else {
				if (board[currentPosition.first][i].isWhite() != white) {
					result.add(new Pair<>(currentPosition.first, i));
				}
				end = true;
			}
		}
		end = false;
		/* w lewo */
		for (int i = currentPosition.second - 1; i > -1 && !end; i--) {
			if (board[currentPosition.first][i] == null) {
				result.add(new Pair<>(currentPosition.first, i));
			} else {
				if (board[currentPosition.first][i].isWhite() != white) {
					result.add(new Pair<>(currentPosition.first, i));
				}
				end = true;
			}
		}
		end = false;
		/* w prawy dół */
		for (int i = 1; currentPosition.second + i < 8 && currentPosition.first + i < 8 && !end;
		     i++) {
			if (board[currentPosition.first + i][currentPosition.second + i] == null) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second + i));
			} else if (board[currentPosition.first + i][currentPosition.second + i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second + i));
				end = true;
			} else {
				end = true;
			}
		}
		end = false;
		/* w lewy dół */
		for (int i = 1; currentPosition.second - i > -1 && currentPosition.first + i < 8 && !end;
		     i++) {
			if (board[currentPosition.first + i][currentPosition.second - i] == null) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second - i));
			} else if (board[currentPosition.first + i][currentPosition.second - i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second - i));
				end = true;
			} else {
				end = true;
			}
		}
		end = false;
		/* w prawą górę */
		for (int i = 1; currentPosition.second + i < 8 && currentPosition.first - i > -1 && !end;
		     i++) {
			if (board[currentPosition.first - i][currentPosition.second + i] == null) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second + i));
			} else if (board[currentPosition.first - i][currentPosition.second + i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second + i));
				end = true;
			} else {
				end = true;
			}
		}
		end = false;
		/* w lewą górę */
		for (int i = 1; currentPosition.second - i > -1 && currentPosition.first - i > -1 &&
				!end; i++) {
			if (board[currentPosition.first - i][currentPosition.second - i] == null) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second - i));
			} else if (board[currentPosition.first - i][currentPosition.second - i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second - i));
				end = true;
			} else {
				end = true;
			}
		}
		return result;
	}

	private LinkedList<Pair<Integer, Integer>> getBishopMoves(Piece[][] board) {
		LinkedList<Pair<Integer, Integer>> result = new LinkedList<>();
		boolean end = false;
		/* w prawy dół */
		for (int i = 1; currentPosition.first + i < 8 && currentPosition.second + i < 8 && !end;
		     i++) {
			if (board[currentPosition.first + i][currentPosition.second + i] == null) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second + i));
			} else if (board[currentPosition.first + i][currentPosition.second + i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second + i));
				end = true;
			} else {
				end = true;
			}
		}
		end = false;
		/* w lewy dół */
		for (int i = 1; currentPosition.second - i > -1 && currentPosition.first + i < 8 && !end;
		     i++) {
			if (board[currentPosition.first + i][currentPosition.second - i] == null) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second - i));
			} else if (board[currentPosition.first + i][currentPosition.second - i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first + i, currentPosition.second - i));
				end = true;
			} else {
				end = true;
			}
		}
		end = false;
		/* w prawą górę */
		for (int i = 1; currentPosition.second + i < 8 && currentPosition.first - i > -1 && !end;
		     i++) {
			if (board[currentPosition.first - i][currentPosition.second + i] == null) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second + i));
			} else if (board[currentPosition.first - i][currentPosition.second + i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second + i));
				end = true;
			} else {
				end = true;
			}
		}
		end = false;
		/* w lewą górę */
		for (int i = 1; currentPosition.second - i > -1 && currentPosition.first - i > -1 &&
				!end; i++) {
			if (board[currentPosition.first - i][currentPosition.second - i] == null) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second - i));
			} else if (board[currentPosition.first - i][currentPosition.second - i].
					isWhite() != white) {
				result.add(new Pair<>(currentPosition.first - i, currentPosition.second - i));
				end = true;
			} else {
				end = true;
			}
		}
		return result;
	}

	public boolean isCheck(Piece[][] board) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (board[i][j] != null) {
					if (board[i][j].getAvailableMoves(board, true).contains(currentPosition)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public int attackPoint(Piece[][] board) {
		int result = 0;
		for (int i = 0; i < availableMoves.size(); i++) {
			Pair<Integer, Integer> pair = availableMoves.get(i);
			if (board[pair.first][pair.second] != null &&
					board[pair.first][pair.second].isWhite() != white) {
				result += board[pair.first][pair.second].value();
			}
		}
		return result;
	}

	public int defencePoint(Piece[][] board) {
		int result = 0;
		for (int i = 0; i < availableMoves.size(); i++) {
			Pair<Integer, Integer> pair = availableMoves.get(i);
			if (board[pair.first][pair.second] != null &&
					board[pair.first][pair.second].isWhite() == white) {
				result += board[pair.first][pair.second].value();
			}
		}
		return result;
	}

	public int value() {
		switch (figureType) {
			case BISHOP:
				return 300;
			case KING:
				return 2000;
			case KNIGHT:
				return 300;
			case PAWN:
				return 100;
			case QUEEN:
				return 900;
			case ROOK:
				return 500;
			default:
				return 0;
		}
	}
}
