package pl.awalas.json.chees.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Pair;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;

import pl.awalas.json.chees.R;
import pl.awalas.json.chees.model.AlfaBeta;
import pl.awalas.json.chees.model.Piece;

/**
 * Created by JSON on 02.06.15.
 */
public class Board extends Activity {

	private TextView[][] board;
	public Piece[][] pieces;
	private Piece blackKing, whiteKing, selected;
	private Display mDisplay;
	private int displayHeight;
	private boolean whiteTurn = true, game = true;
	private TextView firstPlayer, secondPlayer, time, result;
	private long startTime = 0L, timeSwapBuff = 0L, updatedTime = 0L, timeInMilliseconds = 0L;
	private Handler customHandler = new Handler();

	public Board() {
		pieces = new Piece[8][8];
		board = new TextView[8][8];
	}

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_board);
		mDisplay = this.getWindowManager().getDefaultDisplay();
		displayHeight = mDisplay.getWidth();
		createPieces();
		createButton();
		createLayout();
		setInitValue();
		game(MainActivity.firstPlayer, MainActivity.secondPlayer);
	}

	private void createLayout() {
		LinearLayout layout = (LinearLayout) findViewById(R.id.board);
		TableLayout table = new TableLayout(this);
		layout.addView(table);
		table.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		table.setStretchAllColumns(true);
		table.setOrientation(LinearLayout.VERTICAL);
		firstPlayer = new TextView(this);
		secondPlayer = new TextView(this);
		time = new TextView(this);
		result = new TextView(this);
		firstPlayer.setTextSize(20);
		secondPlayer.setTextSize(20);
		time.setTextSize(20);
		result.setTextSize(20);
		firstPlayer.setGravity(Gravity.CENTER_HORIZONTAL);
		secondPlayer.setGravity(Gravity.CENTER_HORIZONTAL);
		time.setGravity(Gravity.CENTER_HORIZONTAL);
		result.setGravity(Gravity.CENTER_HORIZONTAL);
		firstPlayer.setPadding(0, 10, 0, 10);
		secondPlayer.setPadding(0, 10, 0, 10);
		time.setPadding(0, 10, 0, 10);
		result.setPadding(0, 10, 0, 10);
		layout.addView(firstPlayer);
		layout.addView(secondPlayer);
		layout.addView(time);
		layout.addView(result);
		for (int x = 7; x >= 0; x--) {
			TableRow tr = new TableRow(this);
			table.addView(tr);
			tr.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			for (int y = 0; y < 8; y++) {
				TextView tv = board[x][y];
				tv.setTextSize(30);
				tv.setGravity(Gravity.CENTER);
				if (pieces[x][y] != null) {
					tv.setText(pieces[x][y].toString());
				}
				tv.setMinimumHeight(displayHeight / 8);
				tv.setMinimumWidth(20);
				tv.setMaxHeight(displayHeight / 8);
				tv.setMaxWidth(20);
				tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
						TableRow.LayoutParams.WRAP_CONTENT));
				tr.addView(tv);
			}
		}
	}

	private void createButton() {
		for (int x = 7; x >= 0; x--) {
			for (int y = 0; y < 8; y++) {
				board[x][y] = new TextView(this);
				board[x][y].setBackgroundColor((((x + y) % 2) != 0) ? Color.WHITE : Color.GRAY);
			}
		}
	}

	private void setInitValue() {
		firstPlayer.setText("First Player: " +
				getResources().getStringArray(R.array.player)[MainActivity.firstPlayer]);
		secondPlayer.setText("Second Player: " +
				getResources().getStringArray(R.array.player)[MainActivity.secondPlayer]);
		time.setText("Time: 00:00:00");
		startTime = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 0);
		result.setText("Result: ");
	}

	private void createPieces() {
		/* ustawienie pionów */
		for (int x = 0; x < 8; x++) {
			pieces[1][x] = new Piece(Piece.PAWN, true, new Pair<>(1, x));
			pieces[6][x] = new Piece(Piece.PAWN, false, new Pair<>(6, x));
		}
		/* ustawienie pozostałych bierek */
		pieces[0][0] = new Piece(Piece.ROOK, true, new Pair<>(0, 0));
		pieces[7][0] = new Piece(Piece.ROOK, false, new Pair<>(7, 0));
		pieces[0][7] = new Piece(Piece.ROOK, true, new Pair<>(0, 7));
		pieces[7][7] = new Piece(Piece.ROOK, false, new Pair<>(7, 7));
		pieces[0][1] = new Piece(Piece.KNIGHT, true, new Pair<>(0, 1));
		pieces[7][1] = new Piece(Piece.KNIGHT, false, new Pair<>(7, 1));
		pieces[0][6] = new Piece(Piece.KNIGHT, true, new Pair<>(0, 6));
		pieces[7][6] = new Piece(Piece.KNIGHT, false, new Pair<>(7, 6));
		pieces[0][2] = new Piece(Piece.BISHOP, true, new Pair<>(0, 2));
		pieces[7][2] = new Piece(Piece.BISHOP, false, new Pair<>(7, 2));
		pieces[0][5] = new Piece(Piece.BISHOP, true, new Pair<>(0, 5));
		pieces[7][5] = new Piece(Piece.BISHOP, false, new Pair<>(7, 5));
		pieces[0][3] = new Piece(Piece.QUEEN, true, new Pair<>(0, 3));
		pieces[7][3] = new Piece(Piece.QUEEN, false, new Pair<>(7, 3));
		pieces[0][4] = new Piece(Piece.KING, true, new Pair<>(0, 4));
		pieces[7][4] = new Piece(Piece.KING, false, new Pair<>(7, 4));

		/* zapamiętanie pozycji królów */
		blackKing = pieces[0][4];
		whiteKing = pieces[7][4];
	}

	public void game(int firstPlayer, final int secondPlayer) {
		/* uruchomienie algorytmów dla graczy */
		if (firstPlayer == 0 && secondPlayer == 0) {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					final int x = i, y = j;
					board[i][j].setOnClickListener(new View.OnClickListener() {
						@Override public void onClick(View v) {
							if (game) {
								if (selected != null) {
									if (pieces[x][y] == selected) {
										board[x][y].setBackgroundColor(
												(((x + y) % 2) != 0) ? Color.WHITE : Color.GRAY);
										selected = null;
										return;
									} else {
										drawBoard();
										isCheck(whiteTurn, false);
										if (pieces[selected.getCurrentPosition().first][selected
												.getCurrentPosition().second]
												.getAvailableMoves(pieces)
												.contains(new Pair<>(x, y))) {
											int prevX = selected.getCurrentPosition().first;
											int prevY = selected.getCurrentPosition().second;
											pieces[x][y] = pieces[selected
													.getCurrentPosition().first][selected
													.getCurrentPosition().second];
											pieces[x][y].move(Board.this, new Pair<>(x, y));
											pieces[prevX][prevY] = null;
											selected = null;
											if (isCheck(!whiteTurn, true)) {
												Toast.makeText(getApplicationContext(),
														"Ruch nie dozwolony!", Toast.LENGTH_LONG)
														.show();
												pieces[prevX][prevY] = pieces[x][y];
												pieces[prevX][prevY]
														.move(Board.this, new Pair<>(prevX,
																prevY));
												pieces[x][y] = null;
												whiteTurn = !whiteTurn;
											}
											drawBoard();
											isCheck(whiteTurn, false);
											whiteTurn = !whiteTurn;
										}
									}
								} else {
									if (pieces[x][y] != null &&
											pieces[x][y].isWhite() == whiteTurn) {
										selected = pieces[x][y];
										board[x][y].setBackgroundColor(Color.RED);
									}
								}
							}
						}
					});
				}
			}
		}

		if (firstPlayer != 0 && secondPlayer != 0) {
			drawBoard();
			countDown(3000);
		}
	}

	private boolean isCheck(boolean whiteTurn, boolean inMiddle) {
		if (!whiteTurn ? blackKing.isCheck(pieces) : whiteKing.isCheck(pieces)) {
			if (!inMiddle) {
				if (isCheckMate(!whiteTurn ? blackKing : whiteKing)) {
					Toast.makeText(getApplicationContext(), "Szach mat! Koniec gry",
							Toast.LENGTH_LONG).show();
					game = false;
					customHandler.removeCallbacks(updateTimerThread);
				} else {
					Toast.makeText(getApplicationContext(), "Szach!", Toast.LENGTH_LONG).show();
				}
			}
			return true;
		}
		return false;
	}

	private boolean isCheckMate(Piece playerKing) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (pieces[x][y] != null && pieces[x][y].isWhite() == playerKing.isWhite()) {
					LinkedList<Pair<Integer, Integer>> availableMoves =
							pieces[x][y].getAvailableMoves(pieces, true);
					for (int i = 0; i < availableMoves.size(); i++) {
						int nextX = availableMoves.get(i).first;
						int nextY = availableMoves.get(i).second;
						pieces[nextX][nextY] = pieces[x][y];
						pieces[nextX][nextY].move(Board.this, new Pair<>(nextX, nextY), true);
						pieces[x][y] = null;

						boolean haveMove = !playerKing.isCheck(pieces);
						pieces[nextX][nextY].move(Board.this, new Pair<>(x, y), true);
						pieces[x][y] = pieces[nextX][nextY];
						pieces[nextX][nextY] = null;

						if (haveMove) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private void drawBoard() {
		for (int x = 7; x >= 0; x--) {
			for (int y = 0; y < 8; y++) {
				board[x][y].setBackgroundColor((((x + y) % 2) != 0) ? Color.WHITE : Color.GRAY);
				if (pieces[x][y] != null) {
					board[x][y].setText(pieces[x][y].toString());
				} else {
					board[x][y].setText("");
				}
			}
		}
	}

	private Runnable updateTimerThread = new Runnable() {

		public void run() {
			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
			updatedTime = timeSwapBuff + timeInMilliseconds;
			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			time.setText("Time: " + mins + ":" + String.format("%02d", secs) + ":" +
					String.format("%03d", milliseconds));
			customHandler.postDelayed(this, 0);
		}
	};

	private void countDown(int time) {
		new CountDownTimer(time, 1000) {
			public void onTick(long millisUntilFinished) {
				drawBoard();
			}

			public void onFinish() {
				if (game) {
					AlfaBeta alfaBeta = new AlfaBeta();
					drawBoard();
					isCheck(whiteTurn, false);
					drawBoard();
					Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> move =
							alfaBeta.getMove(getBaseContext(), whiteKing, blackKing, whiteTurn,
									whiteTurn ? MainActivity.firstPlayer :
											MainActivity.secondPlayer, pieces);
					pieces[move.second.first][move.second.second] =
							pieces[move.first.first][move.first.second];
					pieces[move.second.first][move.second.second]
							.move(Board.this, new Pair<>(move.second.first, move.second.second));
					pieces[move.first.first][move.first.second] = null;
					drawBoard();
					isCheck(whiteTurn, false);
					whiteTurn = !whiteTurn;
					countDown(3000);
				}
			}
		}.start();
	}
}