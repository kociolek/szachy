package pl.awalas.json.chees.model;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.Pair;

import java.util.LinkedList;

/**
 * Created by JSON on 06.06.15.
 */
public class AlfaBeta {

	private LinkedList<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> promisingMove,
			tempPromisingMove;
	private LinkedList<Integer> promisingPoint, tempPromisingPoint;
	private LinkedList<Piece[][]> promisingBoard, tempPromisingBoard;
	private LinkedList<Pair<Piece, Piece>> promisingKing, tempPromisingKing;
	private Piece[][] originalBoard;
	private Piece whiteKing, blackKing;
	private boolean whiteTurn, haveTime;
	private int deepCount, player;
	private Context context;

	public AlfaBeta() {
	}

	public Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> getMove(Context context,
			Piece whiteKing, Piece blackKing, boolean whiteTurn, int player,
			Piece[][] originalBoard) {
		this.context = context;
		this.whiteKing = whiteKing;
		this.blackKing = blackKing;
		this.whiteTurn = whiteTurn;
		this.player = player;
		this.originalBoard = originalBoard;
		promisingMove = new LinkedList<>();
		promisingPoint = new LinkedList<>();
		promisingBoard = new LinkedList<>();
		promisingKing = new LinkedList<>();
		tempPromisingMove = new LinkedList<>();
		tempPromisingPoint = new LinkedList<>();
		tempPromisingBoard = new LinkedList<>();
		tempPromisingKing = new LinkedList<>();
		searchMove();
		int bestPoint = -1000000000;
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> bestMove = null;
		for (int i = 0; i < promisingMove.size(); i++) {
			if (promisingPoint.get(i) > bestPoint) {
				bestPoint = promisingPoint.get(i);
				bestMove = promisingMove.get(i);
			}
		}
		return bestMove;
	}

	private void searchMove() {
		haveTime = true;
		countDown(5000); // 5s
		alfaBetaMax(whiteKing, blackKing, whiteTurn, originalBoard, null, 0);
		/*for (int i = 0; i < promisingMove.size(); i++) {
			alfaBetaMin(promisingKing.get(i).first, promisingKing.get(i).second, !whiteTurn,
					promisingBoard.get(i), promisingMove.get(i), 0);
		}
		promisingMove = tempPromisingMove;
		promisingPoint = tempPromisingPoint;
		promisingBoard = tempPromisingBoard;
		promisingKing = tempPromisingKing;
		tempPromisingPoint.clear();
		tempPromisingKing.clear();
		tempPromisingBoard.clear();
		tempPromisingMove.clear();
		while (haveTime) {
			deepCount++;
			int number = 0;
			for (int i = 0; i < promisingMove.size(); i++) {
				alfaBetaMax(promisingKing.get(i).first, promisingKing.get(i).second, whiteTurn,
						promisingBoard.get(i), promisingMove.get(i), number++);
				if (number == Math.pow(5, deepCount / 2)) {
					number = 0;
				}
			}
			number = 0;
			promisingMove = tempPromisingMove;
			promisingPoint = tempPromisingPoint;
			promisingBoard = tempPromisingBoard;
			promisingKing = tempPromisingKing;
			tempPromisingPoint.clear();
			tempPromisingKing.clear();
			tempPromisingBoard.clear();
			tempPromisingMove.clear();
			for (int i = 0; i < promisingMove.size(); i++) {
				alfaBetaMin(promisingKing.get(i).first, promisingKing.get(i).second, !whiteTurn,
						promisingBoard.get(i), promisingMove.get(i), number++);
				if (number == Math.pow(5, deepCount / 2)) {
					number = 0;
				}
			}
			promisingMove = tempPromisingMove;
			promisingPoint = tempPromisingPoint;
			promisingBoard = tempPromisingBoard;
			promisingKing = tempPromisingKing;
			tempPromisingPoint.clear();
			tempPromisingKing.clear();
			tempPromisingBoard.clear();
			tempPromisingMove.clear();
		}*/
	}

	private void alfaBetaMax(Piece whiteKing, Piece blackKing, boolean whiteTurn,
			Piece[][] currentBoard, Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> firstMove,
			int number) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				// sprawdzaj tylko dla tego samego koloru dla którego jest ruch
				if (currentBoard[x][y] != null && currentBoard[x][y].isWhite() == whiteTurn) {
					LinkedList<Pair<Integer, Integer>> availableMoves =
							currentBoard[x][y].getAvailableMoves(currentBoard, true);
					for (Pair<Integer, Integer> pair : availableMoves) {
						Piece destination = currentBoard[pair.first][pair.second];
						Log.i("MAX1", String.valueOf(x) + String.valueOf(y) + " " +
								currentBoard[x][y].toString());
						Log.i("MAX1", String.valueOf(pair.first) + String.valueOf(pair.second));
						currentBoard[pair.first][pair.second] = currentBoard[x][y];
						currentBoard[pair.first][pair.second]
								.move(context, new Pair<>(pair.first, pair.second), true);
						if (currentBoard[pair.first][pair.second].getFigureType() == Piece.KING) {
							if (whiteTurn) {
								whiteKing = currentBoard[pair.first][pair.second];
							} else {
								blackKing = currentBoard[pair.first][pair.second];
							}
						}
						currentBoard[x][y] = null;
						int point =
								makeDegree(whiteKing, blackKing, whiteTurn, player, currentBoard);
						boolean inserted = false;
						if (deepCount == 0) {
							int i = 0;
							for (; i < promisingPoint.size() && i < 5 && !inserted; i++) {
								if (promisingPoint.get(i) < point) {
									promisingPoint.add(i, point);
									promisingMove.add(i, new Pair<>(new Pair<>(x, y),
											new Pair<>(pair.first, pair.second)));
									promisingBoard.add(i, currentBoard);
									promisingKing.add(i, new Pair<>(whiteKing, blackKing));
									inserted = true;
								}
							}
							if (!inserted && i < 5) {
								promisingPoint.addLast(point);
								promisingMove.addLast(new Pair<>(new Pair<>(x, y),
										new Pair<>(pair.first, pair.second)));
								promisingBoard.addLast(currentBoard);
								promisingKing.addLast(new Pair<>(whiteKing, blackKing));
							}
						} else {
							boolean found = false;
							for (int i = 0; i < promisingMove.size() && !found; i++) {
								if (promisingMove.get(i).equals(firstMove)) {
									tempPromisingPoint.add(promisingPoint.get(i + number) + point);
									tempPromisingMove.add(firstMove);
									tempPromisingBoard.add(currentBoard);
									tempPromisingKing.add(new Pair<>(whiteKing, blackKing));
									found = true;
								}
							}
						}
						currentBoard[x][y] = currentBoard[pair.first][pair.second];
						currentBoard[x][y].move(context, new Pair<>(x, y), true);
						if (currentBoard[x][y].getFigureType() == Piece.KING) {
							if (whiteTurn) {
								whiteKing = currentBoard[x][y];
							} else {
								blackKing = currentBoard[x][y];
							}
						}
						currentBoard[pair.first][pair.second] = destination;
						Log.i("MAX2", String.valueOf(x) + String.valueOf(y) + " " +
								currentBoard[x][y].toString());
						Log.i("MAX2",
								String.valueOf(pair.first) + String.valueOf(pair.second) + " " +
										currentBoard[x][y].toString());
						blackKing.isCheck(currentBoard);
					}
				}
			}
		}
		Log.i("MAX3", "__________________________________________________________");
	}

	private void alfaBetaMin(Piece whiteKing, Piece blackKing, boolean whiteTurn,
			Piece[][] currentBoard, Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> firstMove,
			int number) {
		Piece[][] resultBoard = null;
		int point = 0;
		Piece resultWhiteKing = null, resultBlackKing = null;
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				// sprawdzaj tylko dla tego samego koloru dla którego jest ruch
				if (currentBoard[x][y] != null && currentBoard[x][y].isWhite() == whiteTurn) {
					LinkedList<Pair<Integer, Integer>> availableMoves =
							currentBoard[x][y].getAvailableMoves(currentBoard, true);
					for (Pair<Integer, Integer> pair : availableMoves) {
						Piece destination = currentBoard[pair.first][pair.second];
						Log.i("MIN1", String.valueOf(x) + String.valueOf(y) + " " +
								currentBoard[x][y].toString());
						Log.i("MIN1", String.valueOf(pair.first) + String.valueOf(pair.second));
						currentBoard[pair.first][pair.second] = currentBoard[x][y];
						currentBoard[pair.first][pair.second]
								.move(context, new Pair<>(pair.first, pair.second), true);
						if (currentBoard[pair.first][pair.second].getFigureType() == Piece.KING) {
							if (whiteTurn) {
								whiteKing = currentBoard[pair.first][pair.second];
							} else {
								blackKing = currentBoard[pair.first][pair.second];
							}
						}
						currentBoard[x][y] = null;
						int tempPoint =
								makeDegree(whiteKing, blackKing, whiteTurn, player, currentBoard);
						if (tempPoint > point) {
							point = tempPoint;
							resultBoard = currentBoard;
							resultBlackKing = blackKing;
							resultWhiteKing = whiteKing;
						}
						currentBoard[x][y] = currentBoard[pair.first][pair.second];
						currentBoard[x][y].move(context, new Pair<>(x, y), true);
						if (currentBoard[x][y].getFigureType() == Piece.KING) {
							if (whiteTurn) {
								whiteKing = currentBoard[x][y];
							} else {
								blackKing = currentBoard[x][y];
							}
						}
						currentBoard[pair.first][pair.second] = destination;
						Log.i("MIN2", String.valueOf(x) + String.valueOf(y) + " " +
								currentBoard[x][y].toString());
						Log.i("MIN2",
								String.valueOf(pair.first) + String.valueOf(pair.second) + " " +
										currentBoard[x][y].toString());
						blackKing.isCheck(currentBoard);
						whiteKing.isCheck(currentBoard);
					}
				}
			}
		}
		boolean found = false;
		Log.i("MAIN", "_____________________________________________________________");

		for (int i = 0; i < promisingMove.size() && !found; i++) {
			if (promisingMove.get(i).equals(firstMove)) {
				tempPromisingPoint.add(promisingPoint.get(i + number) - point);
				tempPromisingMove.add(firstMove);
				tempPromisingBoard.add(resultBoard);
				tempPromisingKing.add(new Pair<>(resultWhiteKing, resultBlackKing));
				found = true;
			}
		}
	}

	public Piece[][] getOriginalBoard() {
		return originalBoard;
	}

	public void setOriginalBoard(Piece[][] originalBoard) {
		this.originalBoard = originalBoard;
	}

	public Piece getWhiteKing() {
		return whiteKing;
	}

	public void setWhiteKing(Piece whiteKing) {
		this.whiteKing = whiteKing;
	}

	public Piece getBlackKing() {
		return blackKing;
	}

	public void setBlackKing(Piece blackKing) {
		this.blackKing = blackKing;
	}

	public boolean isWhiteTurn() {
		return whiteTurn;
	}

	public void setWhiteTurn(boolean whiteTurn) {
		this.whiteTurn = whiteTurn;
	}

	public int getDeepCount() {
		return deepCount;
	}

	public void setDeepCount(int deepCount) {
		this.deepCount = deepCount;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	private void countDown(int time) {
		new CountDownTimer(time, 1000) {
			public void onTick(long millisUntilFinished) {
			}

			public void onFinish() {
				haveTime = false;
			}
		}.start();
	}

	private int makeDegree(Piece whiteKing, Piece blackKing, boolean whiteTurn, int player,
			Piece[][] board) {
		int blackDegree = 0;
		int whiteDegree = 0;
		if (whiteTurn) {
			if (blackKing.isCheck(board)) {
				whiteDegree += 10000;
				if (isCheckMate(blackKing, board)) {
					return 1000000000;
				}
			}
			if (whiteKing.isCheck(board)) {
				whiteDegree -= 1000;
				blackDegree += 1000;
				if (isCheckMate(whiteKing, board)) {
					return -1000000000;
				}
			}
		} else {
			if (whiteKing.isCheck(board)) {
				blackDegree += 10000;
				if (isCheckMate(whiteKing, board)) {
					return 1000000000;
				}
			}
			if (blackKing.isCheck(board)) {
				blackDegree -= 1000;
				whiteDegree += 1000;
				if (isCheckMate(blackKing, board)) {
					return -1000000000;
				}
			}
		}
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (board[x][y] != null) {
					if (board[x][y].isWhite()) {
						if (board[x][y].isCheck(board)) {
							blackDegree += 0.5 * evaluatePiece(board[x][y]);
						}
						switch (player) {
							case 1:
								whiteDegree += board[x][y].attackPoint(board);
								break;
							case 2:
								whiteDegree += board[x][y].defencePoint(board);
								break;
							case 3:
								whiteDegree += board[x][y].attackPoint(board);
								whiteDegree += board[x][y].defencePoint(board);
								break;
						}
						whiteDegree += evaluatePiece(board[x][y]);
						whiteDegree += board[x][y].value();
						whiteDegree += board[x][y].getAvailableMoves(board).size() * 10;
					} else {
						if (board[x][y].isCheck(board)) {
							whiteDegree += 0.5 * evaluatePiece(board[x][y]);
						}
						switch (player) {
							case 1:
								blackDegree += board[x][y].attackPoint(board);
								break;
							case 2:
								blackDegree += board[x][y].defencePoint(board);
								break;
							case 3:
								blackDegree += board[x][y].attackPoint(board);
								blackDegree += board[x][y].defencePoint(board);
								break;
						}
						blackDegree += evaluatePiece(board[x][y]);
						blackDegree += board[x][y].value();
						blackDegree += board[x][y].getAvailableMoves(board).size() * 10;
					}
				}
			}
		}
		return whiteTurn ? whiteDegree - blackDegree : blackDegree - whiteDegree;
	}

	private boolean isCheckMate(Piece playerKing, Piece[][] board) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (board[x][y] != null && board[x][y].isWhite() == playerKing.isWhite()) {
					LinkedList<Pair<Integer, Integer>> availableMoves =
							board[x][y].getAvailableMoves(board, true);
					for (int i = 0; i < availableMoves.size(); i++) {
						int nextX = availableMoves.get(i).first;
						int nextY = availableMoves.get(i).second;
						board[nextX][nextY] = board[x][y];
						board[nextX][nextY].move(context, new Pair<>(nextX, nextY), true);
						board[x][y] = null;

						boolean haveMove = !playerKing.isCheck(board);
						board[nextX][nextY].move(context, new Pair<>(x, y), true);
						board[x][y] = board[nextX][nextY];
						board[nextX][nextY] = null;

						if (haveMove) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	private static final short[] PawnTable =
			new short[]{100, 100, 100, 100, 100, 100, 100, 100, 50, 50, 50, 50, 50, 50, 50, 50, 10,
					10, 20, 30, 30, 20, 10, 10, 5, 5, 10, 27, 27, 10, 5, 5, 0, 0, 0, 25, 25, 0, 0,
					0, 5, -5, -10, 0, 0, -10, -5, 5, 5, 10, 10, -25, -25, 10, 10, 5, 0, 0, 0, 0, 0,
					0, 0, 0};
	private static final short[] KnightTable =
			new short[]{-50, -40, -30, -30, -30, -30, -40, -50, -40, -20, 0, 0, 0, 0, -20, -40,
					-30,
					0, 10, 15, 15, 10, 0, -30, -30, 5, 15, 20, 20, 15, 5, -30, -30, 0, 15, 20, 20,
					15, 0, -30, -30, 5, 10, 15, 15, 10, 5, -30, -40, -20, 0, 5, 5, 0, -20, -40,
					-50,
					-40, -20, -30, -30, -20, -40, -50,};

	private static final short[] BishopTable =
			new short[]{-20, -10, -10, -10, -10, -10, -10, -20, -10, 0, 0, 0, 0, 0, 0, -10, -10, 0,
					5, 10, 10, 5, 0, -10, -10, 5, 5, 10, 10, 5, 5, -10, -10, 0, 10, 10, 10, 10, 0,
					-10, -10, 10, 10, 10, 10, 10, 10, -10, -10, 5, 0, 0, 0, 0, 5, -10, -20, -10,
					-40, -10, -10, -40, -10, -20,};

	private static final short[] KingTable =
			new short[]{-30, -40, -40, -50, -50, -40, -40, -30, -30, -40, -40, -50, -50, -40, -40,
					-30, -30, -40, -40, -50, -50, -40, -40, -30, -30, -40, -40, -50, -50, -40, -40,
					-30, -20, -30, -30, -40, -40, -30, -30, -20, -10, -20, -20, -20, -20, -20, -20,
					-10, 20, 20, 0, 0, 0, 0, 20, 20, 20, 30, 10, 0, 0, 10, 30, 20};

	private static final short[] RookTable =
			new short[]{0, 0, 0, 0, 0, 0, 0, 0, 5, 10, 10, 10, 10, 10, 10, 5, -5, 0, 0, 0, 0, 0, 0,
					-5, -5, 0, 0, 0, 0, 0, 0, -5, -5, 0, 0, 0, 0, 0, 0, -5, -5, 0, 0, 0, 0, 0, 0,
					-5, -5, 0, 0, 0, 0, 0, 0, -5, 0, 0, 0, 5, 5, 0, 0, 0};

	private static final short[] QueenTable =
			new short[]{-20, -10, -10, -5, -5, -10, -10, -20, -10, 0, 0, 0, 0, 0, 0, -10, -10,
					0, 5,
					5, 5, 5, 0, -10, -5, 0, 5, 5, 5, 5, 0, -5, 0, 0, 5, 5, 5, 5, 0, -5, -10, 5, 5,
					5, 5, 5, 0, -10, -10, 0, 5, 0, 0, 0, 0, -10, -20, -10, -10, -5, -5, -10, -10,
					-20};

	public static int evaluatePiece(Piece p) {
		int x = p.getCurrentPosition().first;
		int y = p.getCurrentPosition().second;
		if (!p.isWhite()) {
			x = 7 - x;
		}
		switch (p.getFigureType()) {
			case Piece.BISHOP:
				return BishopTable[x * 8 + y];
			case Piece.KING:
				return KingTable[x * 8 + y];
			case Piece.KNIGHT:
				return KnightTable[x * 8 + y];
			case Piece.PAWN:
				return PawnTable[x * 8 + y];
			case Piece.QUEEN:
				return QueenTable[x * 8 + y];
			case Piece.ROOK:
				return RookTable[x * 8 + y];
			default:
				return 0;
		}
	}
}
