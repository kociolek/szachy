package pl.awalas.json.chees.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import pl.awalas.json.chees.R;


public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {
	public static int firstPlayer = 0;
	public static int secondPlayer = 0;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Spinner spinnerFirst = (Spinner) findViewById(R.id.firstPlayer);

		ArrayAdapter<CharSequence> adapterFirst = ArrayAdapter
				.createFromResource(this, R.array.player, android.R.layout.simple_spinner_item);
		adapterFirst.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerFirst.setAdapter(adapterFirst);
		spinnerFirst.setOnItemSelectedListener(this);

		Spinner spinnerSecond = (Spinner) findViewById(R.id.secondPlayer);

		ArrayAdapter<CharSequence> adapterSecond = ArrayAdapter
				.createFromResource(this, R.array.player, android.R.layout.simple_spinner_item);
		adapterSecond.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerSecond.setAdapter(adapterSecond);
		spinnerSecond.setOnItemSelectedListener(this);
	}

	public void playGame(View view) {
		Intent intent = new Intent(this, Board.class);
		startActivity(intent);
	}

	@Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (parent.getId() == R.id.firstPlayer) {
			firstPlayer = position;
		} else {
			secondPlayer = position;
		}
	}

	@Override public void onNothingSelected(AdapterView<?> parent) {

	}
}